package test;
import  com.youpi.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    @Test
    public void testGetName(){

        User user = new  User("Michel");
        Assertions.assertEquals("Michel",user.getName());
    }
    @Test
    public void testAddUserToGroup(){

        User user1 = new User("Aurélien");
        User user2 = new User("Baptiste");
        User user3 = new User("Sarah");

        List<User> userTestList = new ArrayList<User>(List.of(user1,user2,user3));

        Group group = new Group();

        group.addUserToGroupList(user1);
        group.addUserToGroupList(user2);
        group.addUserToGroupList(user3);

         Assertions.assertEquals( userTestList, group.getGroupList());
    }

    @Test
    public void testIsGroupNumberInputPossible(){
        int groupNumberInput = 6;
        GroupGenerator groupGenerator = new GroupGenerator();

         Assertions.assertEquals(false, groupGenerator.IsGroupNumberInputPossible(groupNumberInput));
    }


    @Test

    public void testCreateRandomizedGroups(){
        int groupNumberInput = 2;
        ArrayList<User> mainUserList = new ArrayList<User>(List.of(new User("Charles"), new User("Marion"), new User("Sarah"),
                new User("Cynthia"), new User("Aurélien"), new User("Michel")));

        CreateRandomizedGroups createRandomizedGroups = new CreateRandomizedGroups();

        Assertions.assertEquals(2,createRandomizedGroups.startRandomize(mainUserList,groupNumberInput).size());
        Assertions.assertNotEquals(mainUserList.get(0),createRandomizedGroups.startRandomize(mainUserList,groupNumberInput).get(0).get(0));
    }
}
