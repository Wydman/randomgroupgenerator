package com.youpi;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private List<User> groupList = new ArrayList<>();

    public void addUserToGroupList(User user) {
        groupList.add(user);
    }

    public List<User> getGroupList() {
        return groupList;
    }
}
