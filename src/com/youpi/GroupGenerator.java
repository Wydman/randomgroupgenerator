package com.youpi;

import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class GroupGenerator {
    /*
    ArrayList<User> mainUserList = new ArrayList<User>(List.of(new User("Charles"), new User("Marion"), new User("Sarah"),
            new User("Cynthia"), new User("Aurélien"), new User("Erwan"),
            new User("Stéphane"), new User("Pierre Alain"), new User("Richard"),
            new User("Maxime"), new User("Joannie"), new User("Baptiste")));
            */
    ArrayList<User> mainUserList = new ArrayList<User>(List.of(new User("Charles"), new User("Marion"), new User("Sarah"),
            new User("Cynthia"), new User("Aurélien"), new User("Michel")));

    public GroupGenerator() {

    }

    public boolean IsGroupNumberInputPossible(int groupNumberInput) {

        if (groupNumberInput >= mainUserList.size()) {
            return false;
        } else {
            return true;
        }
    }
}
